<?php
declare(strict_types=1);

/*
 * This file is part of the website-content-interface-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\WebsiteContentInterface;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Content interface for the common HTML parts
 *
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Html
{

    /**
     * Crawler
     *
     * @var Crawler|null
     */
    private $crawler = null;

    /**
     * Constructor
     *
     * @param Crawler $crawler
     * @throws \LogicException
     */
    final public function __construct(Crawler $crawler)
    {
        if ('' == $crawler->html()) {
            throw new \LogicException('$crawler HTML content must not be empty');
        }

        $this->crawler = $crawler;
    }

    /**
     * Get body tag crawler
     *
     * @return string
     * @throws \LogicException If not found or contained more than once
     */
    public function getBodyTag(): string
    {
        $crawler = $this->crawler->filter('title');
        $count = $crawler->count();
        if (1 != $count) {
            throw new \LogicException('HTML title tag count mismatch 1 != ' . $count);
        }

        return $crawler->text();
    }

    /**
     * Get title tag
     *
     * @return string
     * @throws \LogicException If not found or contained more than once
     */
    public function getTitleTag(): string
    {
        $crawler = $this->crawler->filter('title');
        $count = $crawler->count();
        if (1 != $count) {
            throw new \LogicException('HTML title tag count mismatch 1 != ' . $count);
        }

        return $crawler->text();
    }

    /**
     * Get title tag content
     *
     * @return string
     * @throws \LogicException If not found or contained more than once
     */
    public function getTitleTag(): string
    {
        $crawler = $this->crawler->filter('title');
        $count = $crawler->count();
        if (1 != $count) {
            throw new \LogicException('HTML title tag count mismatch 1 != ' . $count);
        }

        return $crawler->text();
    }
}
